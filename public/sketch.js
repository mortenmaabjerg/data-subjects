//Person 1

let b = function(p) {
    let data;
    let i = 0;
    let x1 = 0
    let x2 = p.windowHeight/8
    let y1 = 0
    let y2 = p.windowHeight-p.windowHeight/8*5

    let week = "10"

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/data-subjects/self_tracking/door_exits_b')
    };

    p.setup = function() {
      p.createCanvas(p.windowWidth/2-5, p.windowHeight/2-5);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

        if(data.exits[i].week != week){
          p.stroke("black")
          p.line(x1, 0, y1, p.windowHeight)
          week = data.exits[i].week
        } else if (data.exits[i].mood === "Positive"){
          x2-=3
          y2+=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Negative"){
          x2+=3
          y2-=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, x2, y1, y2)
        }
        i++;
        x1+=2;
        y1+=2;
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(b, "p1");

//Person 2
let m = function(p) {
    let data;
    let i = 0;
    let x1 = 0
    let x2 = p.windowHeight/8
    let y1 = 0
    let y2 = p.windowHeight-p.windowHeight/8*5

    let week = "10"

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/data-subjects/self_tracking/door_exits_m')
    };

    p.setup = function() {
      p.createCanvas(p.windowWidth/2-5, p.windowHeight/2-5);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

        if(data.exits[i].week != week){
          p.stroke("black")
          p.line(x1, 0, y1, p.windowHeight)
          week = data.exits[i].week
        } else if (data.exits[i].mood === "Positive"){
          x2-=3
          y2+=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Negative"){
          x2+=3
          y2-=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, x2, y1, y2)
        }
        i++;
        x1+=2;
        y1+=2;
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(m, "p2");

// Person 3
let s = function(p) {
    let data;
    let i = 0;
    let x1 = 0
    let x2 = p.windowHeight/8
    let y1 = 0
    let y2 = p.windowHeight-p.windowHeight/8*5

    let week = "10"

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/data-subjects/self_tracking/door_exits_s')
    };

    p.setup = function() {
      p.createCanvas(p.windowWidth/2-5, p.windowHeight/2-5);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

        if(data.exits[i].week != week){
          p.stroke("black")
          p.line(x1, 0, y1, p.windowHeight)
          week = data.exits[i].week
        } else if (data.exits[i].mood === "Positive"){
          x2-=3
          y2+=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Negative"){
          x2+=3
          y2-=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, x2, y1, y2)
        }
        i++;
        x1+=2;
        y1+=2;
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(s, "p3");

//Person 4

let t = function(p) {
    let data;
    let i = 0;
    let x1 = 0
    let x2 = p.windowHeight/8
    let y1 = 0
    let y2 = p.windowHeight-p.windowHeight/8*5

    let week = "10"

    p.preload = function() {
    data = p.loadJSON('https://mortenmaabjerg.gitlab.io/data-subjects/self_tracking/door_exits_t')
    };

    p.setup = function() {
      p.createCanvas(p.windowWidth/2-5, p.windowHeight/2-5);
      p.frameRate(10);
    };

    p.draw = function() {
      try {
        if(data.exits[i].purpose === "Practical"){
          p.stroke("red")
        } else if(data.exits[i].purpose === "Repetition") {
          p.stroke("grey")
        } else if(data.exits[i].purpose === "Leisure"){
          p.stroke("green")
        } else if(data.exits[i].purpose === "Work"){
          p.stroke("blue")
        } else {
          p.stroke("yellow")
        }

        if(data.exits[i].week != week){
          p.stroke("black")
          p.line(x1, 0, y1, p.windowHeight)
          week = data.exits[i].week
        } else if (data.exits[i].mood === "Positive"){
          x2-=3
          y2+=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Negative"){
          x2+=3
          y2-=3
          p.line(x1, x2, y1, y2)
        } else if (data.exits[i].mood === "Neutral"){
          p.line(x1, x2, y1, y2)
        }
        i++;
        x1+=2;
        y1+=2;
    }
    catch(e){
      i++
    }
  }
}
  var myp5 = new p5(t, "p4");
